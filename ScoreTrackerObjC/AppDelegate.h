//
//  AppDelegate.h
//  ScoreTrackerObjC
//
//  Created by Garrick McMickell on 9/13/16.
//  Copyright © 2016 Garrick McMickell. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

