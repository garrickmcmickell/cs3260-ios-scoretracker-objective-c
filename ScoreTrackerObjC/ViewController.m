//
//  ViewController.m
//  ScoreTrackerObjC
//
//  Created by Garrick McMickell on 9/13/16.
//  Copyright © 2016 Garrick McMickell. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    //Left View
    UIView* view = [UIView new];
    view.frame = CGRectZero;
    view.backgroundColor = [UIColor colorWithRed:.2 green:.2 blue:.2 alpha:1];
    view.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:view];
    
    UITextView* txt = [UITextView new];
    txt.backgroundColor = [UIColor colorWithRed:.25 green:.25 blue:.25 alpha:1];
    txt.textColor = [UIColor whiteColor];
    txt.textAlignment = NSTextAlignmentCenter;
    txt.text = @"Team 1";
    txt.translatesAutoresizingMaskIntoConstraints = NO;
    [view addSubview:txt];
    
    lbl = [UILabel new];
    lbl.backgroundColor = [UIColor darkGrayColor];
    lbl.textColor = [UIColor whiteColor];
    lbl.textAlignment = NSTextAlignmentCenter;
    [lbl setFont:[UIFont systemFontOfSize:90]];
    lbl.text = @"0";
    lbl.translatesAutoresizingMaskIntoConstraints = NO;
    [view addSubview:lbl];
    
    step = [UIStepper new];
    step.maximumValue = 21;
    step.tintColor = [UIColor whiteColor];
    step.translatesAutoresizingMaskIntoConstraints = NO;
    [step addTarget:self action:@selector(stepChanged:) forControlEvents:UIControlEventValueChanged];
    [view addSubview:step];
    
    //Right View
    UIView* view1 = [UIView new];
    view1.frame = CGRectZero;
    view1.backgroundColor = [UIColor colorWithRed:.2 green:.2 blue:.2 alpha:1];
    view1.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:view1];
    
    UITextView* txt1 = [UITextView new];
    txt1.backgroundColor = [UIColor colorWithRed:.25 green:.25 blue:.25 alpha:1];
    txt1.textColor = [UIColor whiteColor];
    txt1.textAlignment = NSTextAlignmentCenter;
    txt1.text = @"Team 2";
    txt1.translatesAutoresizingMaskIntoConstraints = NO;
    [view1 addSubview:txt1];
    
    lbl1 = [UILabel new];
    lbl1.backgroundColor = [UIColor darkGrayColor];
    lbl1.textColor = [UIColor whiteColor];
    lbl1.textAlignment = NSTextAlignmentCenter;
    [lbl1 setFont:[UIFont systemFontOfSize:90]];
    lbl1.text = @"0";
    lbl1.translatesAutoresizingMaskIntoConstraints = NO;
    [view1 addSubview:lbl1];
    
    step1 = [UIStepper new];
    step1.maximumValue = 21;
    step1.tintColor = [UIColor whiteColor];
    step1.translatesAutoresizingMaskIntoConstraints = NO;
    [step1 addTarget:self action:@selector(step1Changed:) forControlEvents:UIControlEventValueChanged];
    [view1 addSubview:step1];
    
    //Bottom view
    UIView* view2 = [UIView new];
    view2.frame = CGRectZero;
    view2.backgroundColor = [UIColor colorWithRed:.2 green:.2 blue:.2 alpha:1];
    view2.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:view2];
    
    UIButton* button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [button setTitle:@"Reset" forState:UIControlStateNormal];
    button.tintColor = [UIColor whiteColor];
    button.layer.cornerRadius = 10;
    button.layer.borderWidth = 1;
    button.layer.borderColor = [UIColor whiteColor].CGColor;
    button.translatesAutoresizingMaskIntoConstraints = NO;
    [button addTarget:self action:@selector(buttonClicked) forControlEvents:UIControlEventTouchDown];
    [view2 addSubview:button];
    
    NSDictionary* dictionary = NSDictionaryOfVariableBindings(view, txt, lbl, step, view1, txt1, lbl1, step1, view2, button);
    NSDictionary* metrics = @{};
    
    //Super view constraints
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[view][view1(view)]|" options:0 metrics:metrics views:dictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[view2]|" options:0 metrics:metrics views:dictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[view][view2(100)]|" options:0 metrics:metrics views:dictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[view1][view2(100)]|" options:0 metrics:metrics views:dictionary]];
    
    //Left view constraints
    [view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[txt]|" options:0 metrics:metrics views:dictionary]];
    [view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[lbl]|" options:0 metrics:metrics views:dictionary]];
    [view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-50-[step]-50-|" options:0 metrics:metrics views:dictionary]];
    [view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-20-[txt(30)][lbl]-50-[step]-50-|" options:0 metrics:metrics views:dictionary]];
    
    //Right view constraints
    [view1 addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[txt1]|" options:0 metrics:metrics views:dictionary]];
    [view1 addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[lbl1]|" options:0 metrics:metrics views:dictionary]];
    [view1 addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-50-[step1]-50-|" options:0 metrics:metrics views:dictionary]];
    [view1 addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-20-[txt1(30)][lbl1]-50-[step1]-50-|" options:0 metrics:metrics views:dictionary]];
    
    //Bottom view constraints
    [view2 addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-20-[button]-20-|" options:0 metrics:metrics views:dictionary]];
    [view2 addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-20-[button]-20-|" options:0 metrics:metrics views:dictionary]];
}

-(void)stepChanged:(UIStepper*)stepper{
    lbl.text = [NSString stringWithFormat:@"%.f", stepper.value];
}

-(void)step1Changed:(UIStepper*)stepper{
    lbl1.text = [NSString stringWithFormat:@"%.f", stepper.value];
}

-(void)buttonClicked{
    lbl.text = @"0";
    lbl1.text = @"0";
    step.value = 0;
    step1.value = 0;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
